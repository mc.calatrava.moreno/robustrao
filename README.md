## R/robustrao

[Mar�a del Carmen Calatrava Moreno](http://calatravamoreno.com)  
[Thomas Auzinger](http://auzinger.name)

---

R/robustrao is an [R](http://www.r-project.org) package to compute an extended Rao-Stirling diversity index to handle missing data.

---

### License

This package is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License, version 3, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.  See the GNU
General Public License for more details.

A copy of the GNU General Public License, version 3, is available at
<http://www.r-project.org/Licenses/GPL-3>
